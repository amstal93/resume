# resume

**resume** is a continuous integration pipeline for my public facing resume.

Available at: <https://resume.carstonschilds.ca>


## Build Locally

```bash
docker run -it --rm \
    -v $(pwd):/opt/app \
    registry.gitlab.com/pages/hugo:latest \
    hugo --source /opt/app/html --destination /opt/app/public --minify
```

## Components
### HTML
_html/_ contains a Hugo static site generator configured to produce the resume HTML
### NGINX
_src/nginx.conf_ contains the NGINX configuration which presents the static HTML
### Docker
_Dockerfile_ specifies an NGINX Docker image which is populated with the static HTML and NGINX configuration
### Helm
_chart_ contains a simple set of templates which are used when deploying the NGINX Docker image to Kubernetes
### env
_env_ contains Helm value files for each deployment environment
### CI
_.gitlab-ci.yml_ specifies the continuous integration pipeline stages

## External Dependencies
### cs-hosts
**cs-hosts** is a CI pipeline for configuring my Home Lab's DNS TLD _.cs_. The hostnames used by k8s Ingresses, specified in _env/${env}/values.yaml_ should be defined in that project.
### AWS Route 53 DNS
The production configuration of this project is available externally at resume.carstonschilds.ca, this DNS record is maintained in AWS Route 53.

## Creating Distributable Copy
* Navigate to resume.carstonschilds.ca in Chrome
* Print the webpage
* Choose _Save as PDF_
* Change the margins to custom, 0.3" on all sides seems to work
* Uncheck _Headers and footers_
* Check _Background graphics_
* Save

### Maintainers
* Carston Schilds <carston.schilds@protonmail.com>