FROM klakegg/hugo:alpine as builder
COPY html /var/opt/hugo
WORKDIR /var/opt/hugo
RUN hugo --minify

FROM nginx:alpine
COPY --from=builder /var/opt/hugo/public/*.html /usr/share/nginx/html/
COPY src/nginx.conf /etc/nginx/nginx.conf
