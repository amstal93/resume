# Carston Schilds

[carstonschilds@protonmail.com](mailto:carstonschilds@protonmail.com)
[1 (250) 981-6835](tel:+1-250-981-6825)

---

May 18th, 2021,

To whom it may concern,

I'm excited to be applying for the position of Data Infrastructure Engineer within the Platform and Reliability Engineering department at Skip the Dishes. My aptitude and passion for automation, as well as my experience working in an agile development environment make me an excellent fit for this role.

In my current position as Database Administrator at the University of Northern British Columbia I am a key innovator in an ongoing DevOps transformation. My team and I have implemented agile software development practices, paired programming, code reviews, and other strategies. We have built automated testing and deployment pipelines using Python and Bash scripting, Ansible, Docker, and Kubernetes.

The adoption of these technologies and practices have resulted in a transition from 1-2 costly and high risk deployments of our ERP system per year, with days long maintenance windows, to several releases per hour with no service interruptions. While it has been deeply rewarding to aid in the continued development of the infrastructure at UNBC, I am eager to find new opportunities to hone my skills.

In particular, I hope to take my operational experience with Oracle Databases as a foundation for further developing my skills with MySQL, Postgres, and other database technologies including NoSQL. I am eager to work at scale where technologies such as replication or sharding are required to meet performance and reliability expectations. I am an avid believer in the strengths of open source software to solve novel problems in an enterprise environment. I'm also enthusiastic about leveraging cloud and infrastructure as code technologies to develop maintainable and scalable solutions, which is why I recently achieved the AWS SysOps Administrator Certification.

Thank you for considering my application, I'm looking forward to learning more details about the position,

Sincerely,


Carston Schilds
